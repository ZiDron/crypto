#include "crypt_widget.h"

CryptWidget::CryptWidget(CryptoCipher *cipher, QWidget *parent) {
    this->cipher = cipher;
    setupUi(this);
    verticalLayout->insertWidget(0, &fileChooseWidget, 1);
    connect(importKeyButton, SIGNAL(released()), this, SLOT(importKeySlot()));
    connect(genAndExportKeyButton, SIGNAL(released()), this, SLOT(genAndExportKeySlot()));
    connect(decryptButton, SIGNAL(released()), this, SLOT(decryptFileSlot()));
    connect(encryptButton, SIGNAL(released()), this, SLOT(encryptFileSlot()));
}

CryptWidget::~CryptWidget() {

}

void CryptWidget::updateData() {
    algorithmComboBox->clear();
    for (int i = 0; i < cipher->keyGenAlg.size(); i++) {
        algorithmComboBox->addItem(cipher->keyGenAlg.at(i).second);
    }

    modeComboBox->clear();
    modeComboBox->addItem(trUtf8("Режим сцепления блоков шифротекста (CBC)"), CryptoCipher::CBC_Mode);
    modeComboBox->addItem(trUtf8("Режим обратной связи по выходу (OFB)"), CryptoCipher::OFB_Mode);
    modeComboBox->addItem(trUtf8("Режим обратной связи по шифротексту (CFB)"), CryptoCipher::CFB_Mode);
}

void CryptWidget::importKeySlot() {
    QString keyPath = QFileDialog::getOpenFileName(this, trUtf8("Импортировать ключ"));
    if (!keyPath.isEmpty()) {
        if (cipher->importSessionKey(keyPath.toUtf8().data())) {
            keyPathLineEdit->setText(keyPath);
            sessionKeyIVLabel->setText(QString::fromStdString(cipher->toHexString(cipher->getSessionKeyIV())).toUpper());
            exchangeKeyIVLabel->setText(QString::fromStdString(cipher->toHexString(cipher->getExchangeKeyIV())).toUpper());
            exchangePublicKeyLabel->setText(QString::fromStdString(cipher->toHexString(cipher->getExchangePublicKey())).toUpper());
            modeKeyLabel->setText(modeComboBox->itemText(modeComboBox->findData(cipher->getSessionKeyCipherMode())));
            QMessageBox::information(this, trUtf8("Успех"), trUtf8("Ключ успешно импортирован."));
        } else {
            QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Ошибка импорта ключа."));
        }
    }
}

void CryptWidget::genAndExportKeySlot() {
    QString keyPath = QFileDialog::getSaveFileName(this, trUtf8("Экспортировать ключ"));
    if (!keyPath.isEmpty()) {
        cipher->generateKey(cipher->keyGenAlg.at(algorithmComboBox->currentIndex()).first);
        if (cipher->exportSessionKey(keyPath.toUtf8().data())) {
            if (cipher->importSessionKey(keyPath.toUtf8().data())) {
                keyPathLineEdit->setText(keyPath);
                QMessageBox::information(this, trUtf8("Успех"), trUtf8("Ключ успешно экспортирован и применен."));
            }
        } else {
            QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Ошибка экспорта ключа."));
        }
    }
}

void CryptWidget::decryptFileSlot() {
    QString inFilePath = fileChooseWidget.getInFilePath();
    QString outFilePath = fileChooseWidget.getOutFilePath();
    applyMode();
    if (inFilePath.isEmpty() && !outFilePath.isEmpty()) {
        QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Ошибка выбора файлов!"));
        return;
    }
    if (!cipher->hasKey()) {
        QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Не импортирован ключ!"));
        return;
    }
    if (cipher->decrypt(inFilePath.toUtf8().data(), outFilePath.toUtf8().data())) {
        QMessageBox::information(this, trUtf8("Успех"), trUtf8("Дешифрование успешно!"));
    } else {
        QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Ошибка дешифрования!"));
    }
}

void CryptWidget::encryptFileSlot() {
    QString inFilePath = fileChooseWidget.getInFilePath();
    QString outFilePath = fileChooseWidget.getOutFilePath();
    applyMode();
    if (inFilePath.isEmpty() && !outFilePath.isEmpty()) {
        QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Ошибка выбора файлов!"));
        return;
    }
    if (!cipher->hasKey()) {
        QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Не импортирован ключ!"));
        return;
    }
    if (cipher->encrypt(inFilePath.toUtf8().data(), outFilePath.toUtf8().data())) {
        QMessageBox::information(this, trUtf8("Успех"), trUtf8("Шифрование успешно!"));
    } else {
        QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Ошибка шифрования!"));
    }
}

void CryptWidget::applyMode() {
    bool ok;
    int mode = modeComboBox->itemData(modeComboBox->currentIndex()).toInt(&ok);
    if (ok) {
        printf("mode: %d\n", mode);
        fflush(stdout);
        cipher->setCipherMode((CryptoCipher::CryptMode)mode);
    } else {
        QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Ошибка выбора режима!"));
    }
}
