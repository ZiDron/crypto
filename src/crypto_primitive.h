#ifndef CRYPTO_PRIMITIVE_H
#define CRYPTO_PRIMITIVE_H

#include <iostream>
#include <vector>
#include <iomanip>
#include <wincrypt.h>
#include <cspsdk/importitccsp.h>

class CryptoPrimitive {

public:
    CryptoPrimitive();
    virtual ~CryptoPrimitive();
    virtual bool init(HCRYPTPROV provider);
    void handleError(const char *s);
    std::string toHexString(const std::vector<BYTE> & data);
    HCRYPTPROV provider;

protected:
    DWORD getCipherMode(HCRYPTPROV key);
    std::vector<BYTE> getIV(HCRYPTPROV key);
    std::vector<BYTE> getPublicKey(HCRYPTPROV key);
};

#endif // CRYPTO_PRIMITIVE_H
