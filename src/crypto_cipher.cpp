#include "crypto_cipher.h"

CryptoCipher::CryptoCipher() {

}

bool CryptoCipher::init(HCRYPTPROV provider, QList<QPair<int, QString> > keyGenAlg) {
    CryptoPrimitive::init(provider);
    if (!CryptGetUserKey(provider, AT_KEYEXCHANGE, &baseCryptKey)) {
        std::cout << "CryptGetUserKey AT_KEYEXCHANGE. [Error]\n";
        return false;
    }
    std::cout << "Get user key: AT_KEYEXCHANGE. [ok]\n";
    getCipherMode(baseCryptKey);
    getIV(baseCryptKey);
    getPublicKey(baseCryptKey);
    this->keyGenAlg = keyGenAlg;
    return true;
}

bool CryptoCipher::encrypt(char* srcPath, char* dstPath) {
    // Открытие файла, который будет зашифрован.
    std::cout << "\nOpening source file: " << srcPath << "\n";
    FILE * source = fopen(srcPath, "rb");
    if (!source) {
        handleError("Error opening source file!\n");
        return false;
    }
    std::cout << "File has been opened.\n";

    // Открытие файла, в который будет производится запись зашифрованных данных.
    std::cout << "\nOpening destination file: " << dstPath << "\n";
    FILE * dest = fopen(dstPath, "wb");
    if (!dest) {
        handleError("Error opening destination file!\n");
        return false;
    }
    std::cout << "File has been opened.\n";

    int blockLen = 4096;
    std::cout << "\nEncrypting file.\n";
    do {
        BYTE buf[blockLen] = {0};

        DWORD len = (DWORD)fread(buf, 1, blockLen, source);
        if (!len) {
            handleError("fread() failed.");
            return false;
        }

        BOOL final = feof(source);

        // Шифрование прочитанного блока на сессионном ключе.
        std::cout << "\n   Encrypting " << len << " bytes of data\n";
        if (!CryptEncrypt(sessionKey, 0, final, 0, buf, &len, blockLen)) {
            handleError("CryptEncrypt() failed.\n");
            return false;
        }

        // Запись зашифрованного блока в выходнной файл.
        std::cout << "   Writing " << len << " bytes of encrypted data.\n";
        if (fwrite(buf, 1, len, dest) != len) {
            handleError("fwrite() failed.\n");
            return false;
        }
    }
    while (!feof(source));

    std::cout << "CryptEncrypt success.\n";

    fclose(source);
    fclose(dest);
    return true;
}

bool CryptoCipher::generateKey(ALG_ID genAlg) {
    std::cout << "\nTry generate key.\n";
    if (!CryptGenKey(provider, genAlg, CRYPT_EXPORTABLE | CRYPT_ENCRYPT | CRYPT_DECRYPT, &sessionKey)) {
        std::cerr << "CryptGenKey() failed.";
        handleError("CryptGenKey() failed.");
        return false;
    }
    std::cout << "\nKey has been generated.\n";
    getCipherMode(sessionKey);
    getIV(sessionKey);
    return true;
}

bool CryptoCipher::exportSessionKey(char* dstPath) {
    std::vector<BYTE> receiverPublicKeyBlob = loadPublicKeyInBlob();

    std::cout << "\nImport public key from blob.\n";
    HCRYPTKEY agreeKey = 0;
    if (!CryptImportKey(provider, &receiverPublicKeyBlob[0], receiverPublicKeyBlob.size(), baseCryptKey, 0, &agreeKey)) {
        handleError("CryptImportKey() failed.\n");
        return false;
    }
    std::cout << "Key has been imported: "<< std::endl;
    getCipherMode(agreeKey);
    getIV(agreeKey);
    getPublicKey(agreeKey);

    ALG_ID keAlg = CALG_PRO_EXPORT;
    if (!CryptSetKeyParam(baseCryptKey, KP_EXPORTID, (LPBYTE)&keAlg, 0)) {
        handleError("CryptSetKeyParam() failed.\n");
        return false;
    }

    std::cout << "\nExport sessionKey key.\n";
    DWORD blobSimpleLen = 0;
    if (!CryptExportKey(sessionKey, baseCryptKey, SIMPLEBLOB, 0, nullptr, &blobSimpleLen)) {
        std::cout << "First CryptExportKey() call failed.\n";
        return false;
    }
    std::vector<BYTE> keyBlobSimple(blobSimpleLen);
    if (!CryptExportKey(sessionKey, baseCryptKey, SIMPLEBLOB, 0, &keyBlobSimple[0], &blobSimpleLen)) {
        handleError( "Second CryptExportKey() call failed.\n");
        return false;
    }
    std::cout << "Key has been exported.\n";
    std::cout << toHexString(keyBlobSimple);

    std::cout << "\nImport session key from blob.\n";
    HCRYPTKEY exportedKey = 0;
    if (!CryptImportKey(provider, &keyBlobSimple[0], blobSimpleLen, baseCryptKey, 0, &exportedKey)) {
        handleError("Can`t import exported key");
        return false;
    }

    // Получение синхропосылки.
    std::cout << "\nGetting session key IV .\n";
    DWORD ivLen = 0;
    if (!CryptGetKeyParam(sessionKey, KP_IV, NULL, &ivLen, 0))  {
        handleError("First CryptGetKeyParam() failed.\n");
        return false;
    }
    std::vector<BYTE> ivSessionKey(ivLen);
    if (!CryptGetKeyParam(sessionKey, KP_IV, &ivSessionKey[0], &ivLen, 0)) {
        handleError("Second CryptGetKeyParam() failed.\n");
        return false;
    }
    std::cout << "IV has been obtained.\n";

    // Открытие файла, в который будет производится запись зашифрованных данных.
    std::cout << "\nOpening destination file: " << dstPath << "\n";
    FILE *dst = fopen(dstPath, "wb" );
    if (!dst) {
        handleError("Error opening destination file!\n");
        return false;
    }
    std::cout << "File has been opened.\n";


    // Запись размера сессионного ключа.
    std::cout << "\nWriting session key length.\n";
    if (fwrite(&blobSimpleLen, 1, sizeof(DWORD), dst) != sizeof(DWORD)) {
        handleError("fwrite() failed.\n");
        return false;
    }
    std::cout << "Length has been written.\n";

    // Запись сессионного ключа.
    std::cout << "\nWriting session key.\n";
    if (fwrite(&keyBlobSimple[0], 1, blobSimpleLen, dst) != blobSimpleLen ) {
        handleError("fwrite() failed.\n");
        return false;
    }
    std::cout << "Key has been written.\n";

    // Запись размера синхропосылки.
    std::cout << "\nWriting IV length.\n";
    if (fwrite(&ivLen, 1, sizeof(DWORD), dst) != sizeof(DWORD)) {
        handleError("fwrite() failed.\n");
        return false;
    }
    std::cout << "Length has been written.\n";

    // Запись синхропосылки.
    std::cout << "\nWriting IV.\n";
    if (fwrite( &ivSessionKey[0], 1, ivLen, dst) != ivLen) {
        handleError("fwrite() failed.\n");
        return false;
    }
    std::cout << "IV has been written.\n";
    fclose(dst);
    return true;
}

bool CryptoCipher::decrypt(char *srcPath, char *dstPath) {
    // Открытие файла, который будет расшифрован.
    std::cout << "\nOpening source file: " << srcPath << "\n";
    FILE * source = fopen(srcPath, "rb");
    if (!source) {
        handleError("Error opening source file!\n");
        return false;
    }
    std::cout << "File has been opened.\n";
    // Открытие файла, в который будет производится запись зашифрованных данных.
    std::cout << "\nOpening destination file: " << dstPath << "\n";
    FILE * dest = fopen(dstPath, "wb");
    if (!dest) {
        handleError("Error opening destination file!\n");
        return false;
    }
    std::cout << "File has been opened.\n";


    int blockLen = 4096;
    std::cout << "\nDecrypting file.\n";

    // Поблочное расшифрование входного файла и запись расшифрованных данных в выходной файл.
    //
    std::cout << "\nDecrypting file.\n";
    do {
        BYTE buf[blockLen] = {0};
        DWORD len = (DWORD)fread( buf, 1, blockLen, source );
        if (!len) {
            handleError("fread() failed.");
            return false;
        }

        BOOL final = feof(source);

        // Расшифрование прочитанного блока на сессионном ключе.
        std::cout << "\n   Decrypting " << len << " bytes of data.\n";
        if (!CryptDecrypt(sessionKey, 0, final, 0, buf, &len)) {
            handleError( "CryptDecrypt() failed.");
            return false;
        }

        // Запись расшифрованного блока в файл.
        std::cout << "   Writing " << len << " bytes of decrypted data.\n";
        if (fwrite(buf, 1, len, dest) != len ) {
            handleError("fwrite() failed.");
            return false;
        }
    }
    while(!feof(source));
    fclose(source);
    fclose(dest);
    return true;
}

bool CryptoCipher::importSessionKey(char *srcPath) {
    // Открытие зашифрованного ключа.
    std::cout << "\nOpening source file: " << srcPath << "\n";
    FILE *source = fopen(srcPath, "rb");
    if (!source) {
        handleError( "Error opening source file!\n" );
    }
    std::cout << "File has been opened.\n";

    // Читаем размер сессионного ключа.
    std::cout << "\nReading session key length.\n";
    DWORD blobSimpleLen = 0;
    if (fread(&blobSimpleLen, 1, sizeof(DWORD), source) != sizeof(DWORD)) {
        handleError("fread() failed.");
        return false;
    }
    if (!blobSimpleLen) {
        handleError("Invalid session key length.");
        return false;
    }
    std::cout << "Length has been read: " << blobSimpleLen << "\n";

    // Читаем сессионный ключ.
    std::cout << "\nReading session key.\n";
    std::vector<BYTE> keyBlobSimple(blobSimpleLen);
    if (fread(&keyBlobSimple[0], 1, blobSimpleLen, source) != blobSimpleLen) {
        handleError("fread() failed.");
        return false;
    }
    std::cout << "Session key has been read.\n";

    // Читаем размер синхропосылки для зашифрованного ключа.
    std::cout << "\nReading IV length.\n";
    DWORD ivLen = 0;
    if (fread(&ivLen, 1, sizeof(DWORD), source) != sizeof(DWORD)) {
        handleError("fread() has been failed.");
        return false;
    }
    if (!ivLen) {
        handleError("Invalid IV length.");
        return false;
    }
    std::cout << "Length has been read: " << ivLen << "\n";
    // Читаем синхропосылку.
    std::cout << "\nReading IV.\n";
    std::vector<BYTE> iv( ivLen );
    if (fread(&iv[0], 1, ivLen, source) != ivLen) {
        handleError("fread() failed.");
        return false;
    }
    std::cout << "IV has been read.\n";

    std::vector<BYTE> receiverPublicKeyBlob = loadPublicKeyInBlob();
    std::cout << "\nImport public key from blob.\n";
    HCRYPTKEY agreeKey = 0;
    if (!CryptImportKey(provider, &receiverPublicKeyBlob[0], receiverPublicKeyBlob.size(), baseCryptKey, 0, &agreeKey)) {
        handleError("CryptImportKey() failed.\n");
        return false;
    }
    std::cout << "Key has been imported: "<< std::endl;
    getCipherMode(agreeKey);
    getIV(agreeKey);
    getPublicKey(agreeKey);

    ALG_ID keAlg = CALG_PRO_EXPORT;
    if (!CryptSetKeyParam(baseCryptKey, KP_EXPORTID, (LPBYTE)&keAlg, 0)) {
        handleError("CryptSetKeyParam() failed.\n");
        return false;
    }

    // Получение сессионного ключа импортом зашифрованного сессионного ключа на ключе согласования.
    std::cout << "\nGetting session key.\n";
    if (!CryptImportKey(provider, &keyBlobSimple[0], keyBlobSimple.size(), baseCryptKey, 0, &sessionKey)) {
        handleError("CryptImportKey() failed.");
        return false;
    }
    std::cout << "Key has been acquired.\n";

    // Установка вектора инициализации.
    std::cout << "\nSetting IV.\n";
    if (!CryptSetKeyParam(sessionKey, KP_IV, &iv[0], 0)) {
        handleError("Error during CryptSetKeyParam.");
        return false;
    }
    std::cout << "Parameter has been set.\n";
    fclose(source);
    containerIsImported = true;
    return true;
}

bool CryptoCipher::hasKey() const {
    return containerIsImported;
}

std::vector<BYTE> CryptoCipher::getSessionKeyIV() {
    return getIV(sessionKey);
}

std::vector<BYTE> CryptoCipher::getExchangeKeyIV() {
    return getIV(baseCryptKey);
}

std::vector<BYTE> CryptoCipher::getExchangePublicKey() {
    return getPublicKey(baseCryptKey);
}

DWORD CryptoCipher::getSessionKeyCipherMode() {
    return getCipherMode(sessionKey);
}

bool CryptoCipher::setRandomIVForSessionKey() {
    std::cout << "\nGenerating 8 bytes of random data.\n";
    std::vector<BYTE> randomData(8);
    if (!CryptGenRandom(provider, randomData.size(), &randomData[0])) {
        handleError( "CryptGenRandom() failed.");
        return false;
    }
    std::cout << "Random data: " << toHexString(randomData) << "\n";

    // Установка вектора инициализации.
    std::cout << "\nSetting new IV.\n";
    if (!CryptSetKeyParam(sessionKey, KP_IV, &randomData[0], 0)) {
        handleError("CryptSetKeyParam() failed.\n");
        return false;
    }
    std::cout << "IV has been set.\n";
    return true;
}

std::vector<BYTE> CryptoCipher::loadPublicKeyInBlob() {
    std::cout << "\nLoading public key\n";

    // Запись открытого ключа в ключевой BLOB.
    // Определение размера BLOBа открытого ключа и распределение памяти.
    DWORD keyBlobLen = 0;
    getIV(baseCryptKey);
    if (!CryptExportKey(baseCryptKey, 0, PUBLICKEYBLOB, 0, nullptr, &keyBlobLen)) {
        handleError("First CryptExportKey() call failed.");
    }

    // Экспорт открытого ключа в BLOB открытого ключа.
    std::vector<BYTE> keyBlob(keyBlobLen);
    if (!CryptExportKey(baseCryptKey, 0, PUBLICKEYBLOB, 0, &keyBlob[0], &keyBlobLen)) {
        handleError("Second CryptExportKey() call failed.");
    }

    std::cout << "Key has been loaded, key size: " << keyBlobLen << std::endl;
    std::cout << "key: " << toHexString(keyBlob) << std::endl;
    return keyBlob;
}

void CryptoCipher::setCipherMode(CryptMode mode) {
    if (!CryptSetKeyParam(sessionKey, KP_MODE, (PBYTE)&mode, 0)) {
        handleError("CryptGetKeyParam() failed.");
    }
}

