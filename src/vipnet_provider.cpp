#include "vipnet_provider.h"

ViPNetProvider::ViPNetProvider() {
    providerType.append(QPair<int, QString>(VPN_PROV_TYPE,
                                           QObject::trUtf8( "Тип провайдера на основе алгоритма ГОСТ Р 34.10-2001")));
    providerType.append(QPair<int, QString>(VPN_PROV_TYPE_2012_512,
                                            QObject::trUtf8("Тип провайдера на основе алгоритма ГОСТ Р 34.10-2012 с длиной "
                                            "ключа 256 бит")));
    providerType.append(QPair<int, QString>(VPN_PROV_TYPE_2012_1024,
                                            QObject::trUtf8("Тип провайдера на основе алгоритма ГОСТ Р 34.10-2012 с длиной "
                                            "ключа 512 бит")));


    hashAlgorithms.append(QPair<int, QString>(CPCSP_HASH_ID,
                                              QObject::trUtf8("Алгоритм хэширования в соответствии с ГОСТ Р 34.11-94 "
                                              "с подстановкой \"Верба-О\"")));
    hashAlgorithms.append(QPair<int, QString>(CSP_HASH_2012_256BIT_ID,
                                              QObject::trUtf8("Алгоритм хэширования по ГОСТ Р 34.11-2012 с длиной "
                                              "хэшкода 256 бит")));
    hashAlgorithms.append(QPair<int, QString>(CSP_HASH_2012_512BIT_ID,
                                              QObject::trUtf8("Алгоритм хэширования по ГОСТ Р 34.11-2012 с длиной "
                                              "хэшкода 512 бит")));

//    keyGenAlg.append(QPair<int, QString>(CALG_DH_EL_EPHEM,
//                                         "Создание эфемерной пары ключей по алгоритму ГОСТ Р 34.10-2001"));
//    keyGenAlg.append(QPair<int, QString>(CSP_2012_256_EXCHANGE_ID_EPHEM,
//                                         "Создание эфемерной пары ключей по алгоритму ГОСТ Р 34.10-2012 "
//                                         "с длиной секретного ключа 256 бит"));
//    keyGenAlg.append(QPair<int, QString>(CSP_2012_512_EXCHANGE_ID_EPHEM,
//                                         "Создание эфемерной пары ключей по алгоритму ГОСТ Р 34.10-2012 "
//                                         "с длиной секретного ключа 512 бит"));
//    keyGenAlg.append(QPair<int, QString>(CALG_TLS1_MASTER,
//                                         "Исходный ключ для реализации протокола TLS1"));
    keyGenAlg.append(QPair<int, QString>(CPCSP_ENCRYPT_ID,
                                         QObject::trUtf8("Алгоритм шифрования по ГОСТ 28147-89")));

    signAlgorithms.append(QPair<int, QString>(CPCSP_HASH_ID,
                                             QObject::trUtf8("Алгоритм хэширования в соответствии с ГОСТ Р 34.11-94 "
                                             "с подстановкой \"Верба-О\"")));
    signAlgorithms.append(QPair<int, QString>(CSP_HASH_2012_256BIT_ID,
                                              QObject::trUtf8("Алгоритм хэширования по ГОСТ Р 34.11-2012 с длиной "
                                              "хэшкода 256 бит")));

}
