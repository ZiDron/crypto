#ifndef CRYPT_WIDGET_H
#define CRYPT_WIDGET_H

#include <QWidget>
#include <QFileDialog>
#include "ui_crypt_widget.h"
#include "file_choose_widget.h"
#include <stdio.h>
#include "crypto_cipher.h"

class CryptWidget : public QWidget, public Ui::CryptWidget {
    Q_OBJECT

public:
    explicit CryptWidget(CryptoCipher *cipher, QWidget *parent = 0);
    ~CryptWidget();
    void updateData();

private:
    CryptoCipher *cipher;
    FileChooseWidget fileChooseWidget;

private slots:
    void importKeySlot();
    void genAndExportKeySlot();
    void decryptFileSlot();
    void encryptFileSlot();
    void applyMode();
};

#endif // CRYPT_WIDGET_H
