#ifndef VIPNET_PROVIDER_H
#define VIPNET_PROVIDER_H

#include "crypto_provider.h"

class ViPNetProvider : public CryptoProvider {

public:
    ViPNetProvider();
};

#endif // VIPNET_PROVIDER_H
