#include "crypto_signature.h"

CryptoSignature::CryptoSignature() {

}

bool CryptoSignature::init(HCRYPTPROV provider, QList<QPair<int, QString>> algorithms) {
    this->algorithms = algorithms;
    CryptoPrimitive::init(provider);
    std::cout << "\nAcquiring signature key.\n";
    HCRYPTKEY signatureKey = 0;
    if (!CryptGetUserKey(provider, AT_SIGNATURE, &signatureKey)) {
        std::cout << "Can`t get signature key";
        return false;
    }
    std::cout << "Key has been acquired.\n";

    // Экспорт публичного ключа.
    std::cout << "\nExporting public key blob.\n";
    DWORD publicKeyBlobLen = 0;
    if (!CryptExportKey(signatureKey, 0, PUBLICKEYBLOB, 0, NULL, &publicKeyBlobLen)) {
        std::cout  << "First CryptExportKey() failed.\n";
        return false;
    }

    std::vector<BYTE> publicKeyBlob(publicKeyBlobLen);
    if (!CryptExportKey(signatureKey, 0, PUBLICKEYBLOB, 0, &publicKeyBlob[0], &publicKeyBlobLen)) {
        std::cout  << "Second CryptExportKey() failed.\n";
        return false;
    }
    std::cout << "Key blob has been exported, blob length: " << publicKeyBlobLen << "\n";
    this->publicKeyBlobLen = publicKeyBlobLen;
    this->publicKeyBlob = publicKeyBlob;
    return true;
}

bool CryptoSignature::calculateAndSign(ALG_ID alg, char *data, unsigned int length, char *signature, unsigned int *signatureLength) {
    // Создание объекта хеш.
    std::cout << "\nCreating hash object.\n";
    HCRYPTHASH hash = 0;
    if (!CryptCreateHash(provider, alg, 0, 0, &hash)) {
        std::cout << "CryptCreateHash() failed.\n";
        return false;
    }
    std::cout << "Hash object has been created.\n";

    // Расчет хеш для данных.
    std::cout << "\nCalculating hash.\n";
    if (!CryptHashData(hash, reinterpret_cast<BYTE*>(data), length, 0)) {
        std::cout <<  "CryptHashData() failed.";
        return false;
    }
    std::cout << "Hash has been calculated.\n";

    // Подпись хеша данных.
    std::cout << "\nSigning hash.\n";
    if (!CryptSignHash(hash, AT_SIGNATURE, NULL, 0, NULL, signatureLength)) {
        std::cout << "First CryptSignHash() call failed.\n";
        return false;
    }

    if (!CryptSignHash(hash, AT_SIGNATURE, NULL, 0, reinterpret_cast<BYTE*>(signature), signatureLength)) {
        std::cout << "Second CryptSingHash() call failed.\n";
        return false;
    }
    std::cout << "Hash has been signed, signature length: " << *signatureLength << "\n";
    CryptDestroyHash(hash);
    return true;
}

bool CryptoSignature::calculateAndSign(ALG_ID alg, char *srcFilePath, char *signature, unsigned int *signatureLength) {
    // Создание объекта хеш.
    std::cout << "\nCreating hash object.\n";
    HCRYPTHASH hash = 0;
    if (!CryptCreateHash(provider, alg, 0, 0, &hash)) {
        return false;
    }
    std::cout << "\nOpening source file: " << srcFilePath << "\n";
    FILE * source = fopen(srcFilePath, "rb");
    if (!source) {
        handleError("Error opening source file!\n");
        return false;
    }
    std::cout << "File has been opened.\n";

    int blockLen = 4096;
    do {
        BYTE buf[blockLen] = {0};
        DWORD len = (DWORD)fread(buf, 1, blockLen, source);
        if (!len) {
            handleError("fread() failed.");
            return false;
        }
        if (!CryptHashData(hash, buf, len, 0)) {
            return false;
        }
    }
    while (!feof(source));

    // Подпись хеша данных.
    std::cout << "\nSigning hash.\n";
    if (!CryptSignHash(hash, AT_SIGNATURE, NULL, 0, NULL, signatureLength)) {
        std::cout << "First CryptSignHash() call failed.\n";
        return false;
    }

    if (!CryptSignHash(hash, AT_SIGNATURE, NULL, 0, reinterpret_cast<BYTE*>(signature), signatureLength)) {
        std::cout << "Second CryptSingHash() call failed.\n";
        return false;
    }
    std::cout << "Hash has been signed, signature length: " << *signatureLength << "\n";
    CryptDestroyHash(hash);
    return true;

}

bool CryptoSignature::verifyHash(ALG_ID alg, char *srcFilePath, char *signature, unsigned int signatureLength, bool *result) {
    std::cout << "\nVerifying data hash.\n";

    // Импорт публичного ключа
    std::cout << "\nImporting public key.\n";
    HCRYPTKEY publicKey = 0;
    if (!CryptImportKey(provider, &publicKeyBlob[0], publicKeyBlobLen, 0, 0, &publicKey)) {
        std::cout << "CryptImportKey() failed.";
        return false;
    }
    std::cout << "Key has been imported.\n";

    HCRYPTHASH hash = 0;
    if (!CryptCreateHash(provider, alg, 0, 0, &hash)) {
        return false;
    }
    std::cout << "\nOpening source file: " << srcFilePath << "\n";
    FILE * source = fopen(srcFilePath, "rb");
    if (!source) {
        handleError("Error opening source file!\n");
        return false;
    }
    std::cout << "File has been opened.\n";

    int blockLen = 4096;
    do {
        BYTE buf[blockLen] = {0};
        DWORD len = (DWORD)fread(buf, 1, blockLen, source);
        if (!len) {
            handleError("fread() failed.");
            return false;
        }
        if (!CryptHashData(hash, buf, len, 0)) {
            return false;
        }
    }
    while (!feof(source));


    // Проверка подписи.
    std::cout << "\nVerifying signature.\n";
    if (CryptVerifySignature(hash, reinterpret_cast<BYTE*>(signature), signatureLength, publicKey, NULL, 0)) {
         std::cout << "Signature has been verified.\n";
         *result = true;
    } else {
        std::cout << "Signature is not valid!\n";
        *result = false;
    }
    return true;
}

bool CryptoSignature::verifyHash(ALG_ID alg, char *data, unsigned int length, char *signature, unsigned int signatureLength, bool *result) {
    std::cout << "\nVerifying data hash.\n";

    // Импорт публичного ключа
    std::cout << "\nImporting public key.\n";
    HCRYPTKEY publicKey = 0;
    if (!CryptImportKey(provider, &publicKeyBlob[0], publicKeyBlobLen, 0, 0, &publicKey)) {
        std::cout << "CryptImportKey() failed.";
        return false;
    }
    std::cout << "Key has been imported.\n";

    // Создание объекта хеш.
    std::cout << "\nCreating hash object.\n";
    HCRYPTHASH hash = 0;
    if (!CryptCreateHash(provider, alg, 0, 0, &hash)) {
        std::cout << "CryptCreateHash() failed.";
        return false;
    }
    std::cout << "Hash object has been created.\n";

    // Расчет хеш данных
    std::cout << "\nCalculating hash.\n";
    if (!CryptHashData(hash, reinterpret_cast<BYTE*>(data), length, 0)) {
        std::cout << "CryptHashData() failed.";
        return false;
    }
    std::cout << "Hash has been calculated.\n";


    // Проверка подписи.
    std::cout << "\nVerifying signature.\n";
    if (CryptVerifySignature(hash, reinterpret_cast<BYTE*>(signature), signatureLength, publicKey, NULL, 0)) {
         std::cout << "Signature has been verified.\n";
         *result = true;
    } else {
        std::cout << "Signature is not valid!\n";
        *result = false;
    }
    return true;
}
