#ifndef SIGN_WIDGET_H
#define SIGN_WIDGET_H

#include <QWidget>
#include "crypto_signature.h"
#include "file_choose_widget.h"
#include "ui_sign_widget.h"

class SignWidget : public QWidget, public Ui::SignWidget {
    Q_OBJECT

public:
    explicit SignWidget(CryptoSignature *signature, QWidget *parent = nullptr);
    ~SignWidget();

public slots:
    void updateData();

private:
    CryptoSignature *signature;
    FileChooseWidget *signFileChooseWidget;
    FileChooseWidget *checkFileChooseWidget;

private slots:
    void currentTabChangedSlot(int index);
    void performSignatureOperation();
};

#endif // SIGN_WIDGET_H
