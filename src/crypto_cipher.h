#ifndef CRYPTO_CIPHER_H
#define CRYPTO_CIPHER_H

#include "crypto_primitive.h"
#include <QList>
#include <QPair>

class CryptoCipher : public CryptoPrimitive {

public:
    CryptoCipher();

    enum CryptMode {
        CBC_Mode = 1,
        OFB_Mode = 3,
        CFB_Mode = 4,
    };

    bool init(HCRYPTPROV provider, QList<QPair<int, QString>> keyGenAlg);
    bool encrypt(char *srcPath, char *dstPath);
    bool generateKey(ALG_ID genAlg);
    bool exportSessionKey(char* dstPath);
    bool decrypt(char *srcPath, char *dstPath);
    bool importSessionKey(char* srcPath);
    bool hasKey() const;

    std::vector<BYTE> getSessionKeyIV();
    std::vector<BYTE> getExchangeKeyIV();
    std::vector<BYTE> getExchangePublicKey();

    void setCipherMode(CryptMode mode);
    DWORD getSessionKeyCipherMode();
    bool setRandomIVForSessionKey();

    QList<QPair<int, QString>> keyGenAlg;

private:
    HCRYPTKEY sessionKey = 0;
    HCRYPTKEY baseCryptKey = 0;
    bool containerIsImported = false;

    std::vector<BYTE> loadPublicKeyInBlob();
};

#endif // CRYPTO_CIPHER_H
