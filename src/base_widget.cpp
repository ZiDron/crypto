#include "base_widget.h"

BaseWidget::BaseWidget(CryptoProvider *provider, QWidget *parent) :
    hashWidget(&provider->hash),
    cryptWidget(&provider->cipher),
    sighWidget(&provider->sign),
    containerWidget(provider)
{
    setupUi(this);
    tabWidget->addTab(&cryptWidget, trUtf8("Шифрование"));
    tabWidget->addTab(&hashWidget, trUtf8("Хэширование"));
    tabWidget->addTab(&sighWidget, trUtf8("Подпись"));

    QMenuBar *menuBar = new QMenuBar(this);
    verticalLayout->insertWidget(0, menuBar);
    QMenu *fileMenu = menuBar->addMenu(trUtf8("Файл"));
    fileMenu->addAction(trUtf8("Контейнеры"), this, SLOT(openContainerSettings()));
    fileMenu->addAction(trUtf8("Выход"), QApplication::instance(), SLOT(quit()));

    containerWidget.updateData();
    openContainerSettings();
    if (!provider->hasContainer()) {
        exit(0);
    }
}

BaseWidget::~BaseWidget() {

}

void BaseWidget::openContainerSettings() {
    containerWidget.exec();
    hashWidget.updateData();
    sighWidget.updateData();
    cryptWidget.updateData();
}
