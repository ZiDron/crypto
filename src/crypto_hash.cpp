#include "crypto_hash.h"

CryptoHash::CryptoHash() {

}

bool CryptoHash::init(HCRYPTPROV provider, QList<QPair<int, QString>> hashAlgorithms) {
    CryptoPrimitive::init(provider);
    this->hashAlgorithms = hashAlgorithms;
    return true;
}

bool CryptoHash::calculate(ALG_ID alg, char *data, unsigned int length, char *outBuffer, unsigned int *outLength) {
    if (!CryptCreateHash(provider, alg, 0, 0, &hashContext)) {
        return false;
    }
    if (CryptHashData(hashContext, reinterpret_cast<BYTE*>(data), length, 0)) {
        DWORD dwCount = sizeof(*outLength);
        if (CryptGetHashParam(hashContext, HP_HASHSIZE, reinterpret_cast<BYTE*>(outLength), &dwCount, 0)) {
            if (CryptGetHashParam(hashContext, HP_HASHVAL, reinterpret_cast<BYTE*>(outBuffer), outLength, 0)) {
                CryptDestroyHash(hashContext);
                return true;
            }
        }
    }
    return false;
}

bool CryptoHash::calculate(ALG_ID alg, char *srcFilePath, char *outBuffer, unsigned int *outLength) {
    if (!CryptCreateHash(provider, alg, 0, 0, &hashContext)) {
        return false;
    }
    std::cout << "\nOpening source file: " << srcFilePath << "\n";
    FILE * source = fopen(srcFilePath, "rb");
    if (!source) {
        handleError("Error opening source file!\n");
        return false;
    }
    std::cout << "File has been opened.\n";

    int blockLen = 4096;
    do {
        BYTE buf[blockLen] = {0};
        DWORD len = (DWORD)fread(buf, 1, blockLen, source);
        if (!len) {
            handleError("fread() failed.");
            return false;
        }
        if (!CryptHashData(hashContext, buf, len, 0)) {
            return false;
        }
    }
    while (!feof(source));

    DWORD dwCount = sizeof(*outLength);
    if (CryptGetHashParam(hashContext, HP_HASHSIZE, reinterpret_cast<BYTE*>(outLength), &dwCount, 0)) {
        if (CryptGetHashParam(hashContext, HP_HASHVAL, reinterpret_cast<BYTE*>(outBuffer), outLength, 0)) {
            CryptDestroyHash(hashContext);
            fclose(source);
            return true;
        }
    }
    fclose(source);
    return false;
}
