#include "file_choose_widget.h"

FileChooseWidget::FileChooseWidget(QWidget *parent) : QWidget(parent) {
    setupUi(this);
    connect(inFilePathButton, SIGNAL(released()), this, SLOT(chooseInFilePath()));
    connect(outFilePathButton, SIGNAL(released()), this, SLOT(chooseOutFilePath()));
    connect(swapButton, SIGNAL(released()), this, SLOT(swapFilePaths()));
}

FileChooseWidget::~FileChooseWidget() {
}

QString FileChooseWidget::getInFilePath() const {
    return inFilePathLineEdit->text();
}

QString FileChooseWidget::getOutFilePath() const {
    return outFilePathLineEdit->text();
}

void FileChooseWidget::setOutFileLineHidden(bool hidden) {
    outFilePathButton->setHidden(hidden);
    outFilePathLineEdit->setHidden(hidden);
    swapButton->setHidden(hidden);
}

void FileChooseWidget::chooseInFilePath() {
    QString filePath = QFileDialog::getOpenFileName(this, trUtf8("Выберите входной файл"));
    if (!filePath.isEmpty()) {
        inFilePathLineEdit->setText(filePath);
    } else {
        QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Входной файл задан не верно!"));
    }
}

void FileChooseWidget::chooseOutFilePath() {
    QString filePath = QFileDialog::getSaveFileName(this, trUtf8("Выберите путь выходного файла"));
    if (!filePath.isEmpty()) {
        outFilePathLineEdit->setText(filePath);
    } else {
        QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Выходной файл задан не верно!"));
    }
}

void FileChooseWidget::swapFilePaths() {
    QString inFilePath = inFilePathLineEdit->text();
    inFilePathLineEdit->setText(outFilePathLineEdit->text());
    outFilePathLineEdit->setText(inFilePath);
}
