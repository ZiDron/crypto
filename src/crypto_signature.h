#ifndef CRYPTO_SIGNATURE_H
#define CRYPTO_SIGNATURE_H

#include "crypto_primitive.h"
#include <QList>
#include <QPair>

class CryptoSignature : public CryptoPrimitive {

public:
    CryptoSignature();
    bool init(HCRYPTPROV provider, QList<QPair<int, QString>> algorithms);
    bool calculateAndSign(ALG_ID alg, char *data, unsigned int length, char *signature, unsigned int *signatureLength);
    bool calculateAndSign(ALG_ID alg, char *srcFilePath, char *signature, unsigned int *signatureLength);
    bool verifyHash(ALG_ID alg, char *data, unsigned int length, char *signature, unsigned int signatureLength, bool *result);
    bool verifyHash(ALG_ID alg, char *srcFilePath, char *signature, unsigned int signatureLength, bool *result);

    QList<QPair<int, QString>> algorithms;

private:
    HCRYPTKEY signatureKey = 0;
    DWORD publicKeyBlobLen;
    std::vector<BYTE> publicKeyBlob;
};

#endif // CRYPTO_SIGNATURE_H
