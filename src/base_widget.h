#ifndef BASE_WIDGET_H
#define BASE_WIDGET_H

#include <QWidget>
#include "ui_widget.h"
#include "crypt_widget.h"
#include "hash_widget.h"
#include "sign_widget.h"
#include "container_widget.h"
#include "crypto_provider.h"
#include <QMenuBar>

class BaseWidget : public QWidget, public Ui::Widget {
    Q_OBJECT

public:
    explicit BaseWidget(CryptoProvider* provider, QWidget *parent = nullptr);
    ~BaseWidget();

public slots:
    void openContainerSettings();

private:
    CryptWidget cryptWidget;
    HashWidget hashWidget;
    SignWidget sighWidget;
    ContainerWidget containerWidget;
};

#endif // BASE_WIDGET_H
