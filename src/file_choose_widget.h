#ifndef FILE_CHOOSE_WIDGET_H
#define FILE_CHOOSE_WIDGET_H

#include <QWidget>
#include "ui_file_choose_widget.h"
#include <QFileDialog>
#include <QMessageBox>

class FileChooseWidget : public QWidget, public Ui::FileChooseWidget {
    Q_OBJECT

public:
    explicit FileChooseWidget(QWidget *parent = nullptr);
    ~FileChooseWidget();
    QString getInFilePath() const;
    QString getOutFilePath() const;
    void setOutFileLineHidden(bool hidden);

public slots:
    void chooseInFilePath();
    void chooseOutFilePath();

private slots:
    void swapFilePaths();
};

#endif // FILE_CHOOSE_WIDGET_H
