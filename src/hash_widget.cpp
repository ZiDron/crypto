#include "hash_widget.h"
#include <QDebug>
#include <QComboBox>

HashWidget::HashWidget(CryptoHash *hash, QWidget *parent) : QWidget(parent) {
    setupUi(this);
    this->hash = hash;
    fileChooseWidget = new FileChooseWidget;
    fileChooseWidget->setOutFileLineHidden(true);
    textPlainTextEdit = new QPlainTextEdit;
    hexPlainTextEdit = new QPlainTextEdit;
    tabWidget->addTab(fileChooseWidget, trUtf8("Файл"));
    tabWidget->addTab(textPlainTextEdit, trUtf8("Текст"));
    tabWidget->addTab(hexPlainTextEdit, trUtf8("Hex"));
    connect(hashPushButton, SIGNAL(released()), this, SLOT(performHash()));
}

HashWidget::~HashWidget(){

}

void HashWidget::updateData() {
    comboBox->clear();
    for (int i = 0; i < hash->hashAlgorithms.size(); i++) {
        comboBox->addItem(hash->hashAlgorithms.at(i).second);
    }
}

void HashWidget::performHash() {
    ALG_ID alg = hash->hashAlgorithms.at(comboBox->currentIndex()).first;
    static unsigned int outLen = 0;
    static char outBuffer[512];
    switch (tabWidget->currentIndex()) {
    case 0: {
        QString inFilePath = fileChooseWidget->getInFilePath();
        if (!inFilePath.isEmpty() && hash->calculate(alg, inFilePath.toUtf8().data(), outBuffer, &outLen)) {
            QByteArray outByteArray(outBuffer, outLen);
            hashLineEdit->setText(outByteArray.toHex().toUpper());
        } else {
            showError();
        }
    }
        break;
    case 1: {
        QByteArray textByteArray = textPlainTextEdit->toPlainText().toUtf8();
        char *textPointer = textByteArray.data();
        unsigned int len = textByteArray.size();
        if (hash->calculate(alg, textPointer, len, outBuffer, &outLen)) {
            QByteArray outByteArray(outBuffer, outLen);
            hashLineEdit->setText(outByteArray.toHex().toUpper());
        } else {
            showError();
        }
    }
        break;
    case 2: {
        QByteArray hexByteArray = QByteArray::fromHex(hexPlainTextEdit->toPlainText().toUtf8());
        char *textPointer = hexByteArray.data();
        unsigned int len = hexByteArray.size();
        if (hash->calculate(alg, textPointer, len, outBuffer, &outLen)) {
            QByteArray outByteArray(outBuffer, outLen);
            hashLineEdit->setText(outByteArray.toHex().toUpper());
        } else {
            showError();
        }
    }
        break;
    default:
        break;
    }
}

void HashWidget::showError()  {
    QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Ошибка хэширования!"));
}
