#ifndef CRYPTO_HASH_H
#define CRYPTO_HASH_H

#include "crypto_primitive.h"
#include <wincrypt.h>
#include <cspsdk/importitccsp.h>
#include <vector>
#include <QList>
#include <QPair>

class CryptoHash : public CryptoPrimitive {

public:
    CryptoHash();
    bool init(HCRYPTPROV provider, QList<QPair<int, QString>> hashAlgorithms);
    bool calculate(ALG_ID alg, char *data, unsigned int length, char *outBuffer, unsigned int *outLength);
    bool calculate(ALG_ID alg, char *srcFilePath, char *outBuffer, unsigned int *outLength);
    QList<QPair<int, QString>> hashAlgorithms;
private:
    HCRYPTHASH hashContext;
};

#endif // CRYPTO_HASH_H
