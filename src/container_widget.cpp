#include "container_widget.h"
#include "ui_container_widget.h"

ContainerWidget::ContainerWidget(CryptoProvider *provider, QWidget *parent) : QDialog(parent) {
    setupUi(this);
    this->provider = provider;
    connect(openExistingContainerButton, SIGNAL(released()), this, SLOT(openExistingContainerSlot()));
    connect(newContainerButton, SIGNAL(released()), this, SLOT(createContainerSlot()));
    connect(closeButton, SIGNAL(released()), this, SLOT(close()));
}

ContainerWidget::~ContainerWidget() {

}

void ContainerWidget::updateData() {
    newContainerTypeComboBox->clear();
    for (int i = 0; i < provider->providerType.size(); i++) {
        newContainerTypeComboBox->addItem(provider->providerType.at(i).second);
    }
}

void ContainerWidget::openExistingContainerSlot() {
    QString filePath = QFileDialog::getOpenFileName(this, trUtf8("Открыть существующий контейнер"));
    if (!filePath.isEmpty()) {
        if (provider->init(filePath.toUtf8().data())) {
            existingContainerPathLineEdit->setText(filePath);
            QMessageBox::information(this, trUtf8("Успех"), trUtf8("Контейнер успешно импортирован."));
            close();
        } else {
            QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Ошибка импорта контейнера."));
        }
    }
}

void ContainerWidget::createContainerSlot() {
    QString filePath = QFileDialog::getSaveFileName(this, trUtf8("Создать новый контейнер"));
    if (!filePath.isEmpty()) {
        if (provider->createContainer(filePath.toUtf8().data(), provider->providerType.at(newContainerTypeComboBox->currentIndex()).first)) {
            QMessageBox::information(this, trUtf8("Успех"), trUtf8("Контейнер успешно сохранен."));
        } else {
            QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Ошибка сохранения контейнера."));
        }
    }
}
