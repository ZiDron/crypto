#ifndef HASH_WIDGET_H
#define HASH_WIDGET_H

#include <QWidget>
#include "ui_hash_widget.h"
#include "file_choose_widget.h"
#include <QPlainTextEdit>
#include "crypto_hash.h"

class HashWidget : public QWidget, public Ui::HashWidget {
    Q_OBJECT

public:
    explicit HashWidget(CryptoHash *hash, QWidget *parent = nullptr);
    ~HashWidget();
    void updateData();

private:
    CryptoHash *hash;
    FileChooseWidget *fileChooseWidget;
    QPlainTextEdit *textPlainTextEdit;
    QPlainTextEdit *hexPlainTextEdit;

private slots:
    void performHash();
    void showError();
};

#endif // HASH_WIDGET_H
