#include "sign_widget.h"
#include "ui_sign_widget.h"

SignWidget::SignWidget(CryptoSignature *signature, QWidget *parent) : QWidget(parent) {
    setupUi(this);
    this->signature = signature;
    signFileChooseWidget = new FileChooseWidget;
    signFileChooseWidget->setOutFileLineHidden(true);
    checkFileChooseWidget = new FileChooseWidget;
    checkFileChooseWidget->setOutFileLineHidden(true);
    tabWidget->addTab(signFileChooseWidget, trUtf8("Подпись"));
    tabWidget->addTab(checkFileChooseWidget, trUtf8("Проверка"));
    currentTabChangedSlot(tabWidget->currentIndex());
    connect(tabWidget, SIGNAL(currentChanged(int)), this, SLOT(currentTabChangedSlot(int)));
    connect(performButton, SIGNAL(released()), this, SLOT(performSignatureOperation()));
}

SignWidget::~SignWidget() {

}

void SignWidget::updateData() {
    comboBox->clear();
    for (int i = 0; i < signature->algorithms.size(); i++) {
        comboBox->addItem(signature->algorithms.at(i).second);
    }
}

void SignWidget::currentTabChangedSlot(int index) {
    switch (index) {
    case 0:
        performButton->setText(trUtf8("Подписать"));
        break;
    case 1:
        performButton->setText(trUtf8("Проверить"));
        break;
    default:
        break;
    }
}

void SignWidget::performSignatureOperation() {
    ALG_ID alg = signature->algorithms.at(comboBox->currentIndex()).first;
    static unsigned int outLen = 0;
    static char outBuffer[512];
    switch (tabWidget->currentIndex()) {
    case 0: {
        QString inFilePath = signFileChooseWidget->getInFilePath();
        if (!inFilePath.isEmpty() && signature->calculateAndSign(alg, inFilePath.toUtf8().data(), outBuffer, &outLen)) {
            QByteArray outByteArray(outBuffer, outLen);
            signLineEdit->setText(outByteArray.toHex().toUpper());
        }
    }
        break;
    case 1: {
        QString inFilePath = checkFileChooseWidget->getInFilePath();
        QByteArray signatureByteArray = QByteArray::fromHex(signLineEdit->text().toUtf8());
        char* signaturePtr = signatureByteArray.data();
        unsigned int signatureLen = signatureByteArray.size();
        bool result = false;
        if (!inFilePath.isEmpty() && signature->verifyHash(alg, inFilePath.toUtf8().data(), signaturePtr, signatureLen, &result)) {
            if (result) {
                QMessageBox::information(this, trUtf8("Успех"), trUtf8("Подпись проверена успешно!"));
            } else {
                QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Подпись не прошла проверку!"));
            }
        }
    }
        break;
    default:
        break;
    }

}
