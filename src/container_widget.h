#ifndef CONTAINER_WIDGET_H
#define CONTAINER_WIDGET_H

#include <QWidget>
#include <QFileDialog>
#include <QMessageBox>
#include "ui_container_widget.h"
#include "crypto_provider.h"

class ContainerWidget : public QDialog, public Ui::ContainerWidget {
    Q_OBJECT

public:
    explicit ContainerWidget(CryptoProvider *provider, QWidget *parent = nullptr);
    ~ContainerWidget();
    void updateData();

private slots:
    void openExistingContainerSlot();
    void createContainerSlot();

private:
    CryptoProvider *provider;
};

#endif // CONTAINER_WIDGET_H
