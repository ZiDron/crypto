#-------------------------------------------------
#
# Project created by QtCreator 2018-12-12T08:19:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = crypto
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
    crypt_widget.cpp \
    crypto_provider.cpp \
    hash_widget.cpp \
    crypto_hash.cpp \
    crypto_primitive.cpp \
    crypto_cipher.cpp \
    file_choose_widget.cpp \
    crypto_signature.cpp \
    vipnet_provider.cpp \
    sign_widget.cpp \
    container_widget.cpp \
    base_widget.cpp

HEADERS += \
    crypt_widget.h \
    crypto_provider.h \
    hash_widget.h \
    crypto_hash.h \
    crypto_primitive.h \
    crypto_cipher.h \
    file_choose_widget.h \
    crypto_signature.h \
    vipnet_provider.h \
    sign_widget.h \
    container_widget.h \
    base_widget.h

FORMS += \
        widget.ui \
    crypt_widget.ui \
    hash_widget.ui \
    file_choose_widget.ui \
    sign_widget.ui \
    container_widget.ui

# ViPNet
INCLUDEPATH += "/opt/itcs/include"
LIBS += -L"/opt/itcs/lib"
LIBS += -ladvapi32 -lcrypt32 -lkernel32
QMAKE_LFLAGS += "-Wl,-rpath=/opt/itcs/lib"
