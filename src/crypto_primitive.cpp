#include "crypto_primitive.h"

CryptoPrimitive::CryptoPrimitive() {

}

CryptoPrimitive::~CryptoPrimitive() {

}

bool CryptoPrimitive::init(HCRYPTPROV provider) {
    this->provider = provider;
    return true;
}

void CryptoPrimitive::handleError(const char *s) {
    std::cerr << "\nAn error occurred in running the program.\n";
    std::cerr << s << "\n";
    std::cerr << "Error number 0x" << std::hex << GetLastError() << "\n";
    std::cerr << "Program terminating.\n";
}

std::string CryptoPrimitive::toHexString(const std::vector<BYTE> &data) {
    std::ostringstream ss;
    ss << std::setfill('0') << std::setw(2) << std::hex << std::uppercase;
    for (size_t i = 0; i < data.size(); ++i ) {
        ss << (int)data[i] << " ";
    }
    return ss.str();
}

std::vector<BYTE> CryptoPrimitive::getIV(HCRYPTPROV key) {
    // Чтение вектора инициализации.
    DWORD ivLen = 0;
    if (!CryptGetKeyParam(key, KP_IV, nullptr, &ivLen, 0)) {
        handleError("First CryptGetKeyParam() call failed.\n");
    }

    std::vector<BYTE> iv(ivLen);
    if (!CryptGetKeyParam(key, KP_IV, &iv[0], &ivLen, 0)) {
        handleError("Second CryptGetKeyParam() call failed.\n");
    }
    std::cout << "IV: " << toHexString(iv) << std::endl;
    return iv;
}

DWORD CryptoPrimitive::getCipherMode(HCRYPTPROV key) {
    DWORD mode = 0;
    DWORD modeLen = sizeof( DWORD );
    if (!CryptGetKeyParam(key, KP_MODE, (PBYTE)&mode, &modeLen, 0)) {
        handleError("CryptGetKeyParam() failed.");
    }
    std::cout << "Cipher mode: " << mode << std::endl;
    return mode;
}

std::vector<BYTE> CryptoPrimitive::getPublicKey(HCRYPTPROV key) {
    DWORD keyLen = 0;
    if (!CryptGetKeyParam(key, KP_Y, nullptr, &keyLen, 0)) {
        handleError("CryptGetKeyParam() failed.");
    }
    std::vector<BYTE> strKey(keyLen);
    if (!CryptGetKeyParam(key, KP_Y, &strKey[0], &keyLen, 0)) {
        handleError("CryptGetKeyParam() failed.");
    }
    std::cout << "Public key: " << toHexString(strKey) << std::endl;
    return strKey;
}
