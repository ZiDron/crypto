#include "crypto_provider.h"

CryptoProvider::CryptoProvider() {

}

bool CryptoProvider::init(char* containerName) {
    if (!CryptAcquireContext(&provider, containerName, VPN_DEF_PROV, VPN_PROV_TYPE, 0)) {
        std::cout << "CryptAcquireContext() failed.";
        return false;
    }
    std::cout << "Context has been acquired.\n\n";
    cipher.init(provider, keyGenAlg);
    hash.init(provider, hashAlgorithms);
    sign.init(provider, signAlgorithms);
    containerIsImported = true;
    return true;
}

bool CryptoProvider::createContainer(char *containerName, int type) {
    HCRYPTPROV providerForCreateContainer = 0;
    if (CryptAcquireContext(&providerForCreateContainer, containerName, 0, type,  CRYPT_SILENT)) {
        std::cout << "Context has been acquired.\n";
        /// уже создан
        return false;
    }
    const DWORD lastError = GetLastError();

    // При получении контекста возникла ошибка. Наиболее вероятная причина - контейнер с заданным именем не
    // существует.
    std::cout << "Failed to acquire context.\n";

    // Доработка Инфотекс. В оригинальном примере Microsoft проверяется только флаг NTE_BAD_KEYSET
    if (lastError == NTE_BAD_KEYSET || lastError == NTE_KEYSET_NOT_DEF || lastError == NTE_BAD_KEYSET_PARAM || lastError == ERROR_CANCELLED) {
        // Создаем новый контейнер.
        std::cout << "\nCreating new key container.\n";

        if (!CryptAcquireContext(&providerForCreateContainer, containerName, 0, type,  CRYPT_NEWKEYSET)) {
            std::cout << "Could not create a new key container.\n";
            return false;
        }
        std::cout << "A new key container has been created.\n";
    } else {
        return false;
    }

    // Дескриптор пары ключей.
    HCRYPTKEY signatureKey = 0;
    // Дескриптор крипто провайдера получен.
    // Пытаемся получить дескриптор ключа подписи.
    std::cout << "\nAcquiring signature key.\n";

    if (CryptGetUserKey(providerForCreateContainer, AT_SIGNATURE, &signatureKey)) {
        std::cout << "A signature key is available.\n";
    } else {
        std::cout << "No signature key is available.\n";
        if (GetLastError() == NTE_NO_KEY) {
            // Ключи подписи отсутствуют в контейнере.
            // Создаем пару ключей подписи.
            std::cout << "\nCreating signature key pair.\n";

            if (CryptGenKey(providerForCreateContainer, AT_SIGNATURE, 0, &signatureKey)) {
                std::cout << "Signature key pair created.\n";
            } else {
                std::cout << "Error creating signature key.\n";
                return false;
            }
        } else {
            std::cout << "An error other than NTE_NO_KEY at CryptGetUserKey().";
            return false;
        }
    }

    // Освобождаем дескриптор ключа подписи.
    std::cout << "\nDestroing signature key.\n";

    if (!CryptDestroyKey(signatureKey)) {
        std::cout << "Error at CryptDestroyKey().";
        return false;
    }
    signatureKey = 0;
    std::cout << "Key has been destroyed.\n";

    // Дескриптор пары ключей.
    HCRYPTKEY exchangeKey = 0;

    // Пытаемся получить ключи обмена.
    std::cout << "\nAcquiring exchange key.\n";

    if (CryptGetUserKey(providerForCreateContainer, AT_KEYEXCHANGE, &exchangeKey)) {
        std::cout << "An exchange key is available.\n";
    } else {
        std::cout << "No exchange key is available.\n";

        if (GetLastError() == NTE_NO_KEY) {
            // Ключи обмена отсутствуют в контейнере.
            // Создаем пару ключей обмена.
            std::cout << "\nCreating exchange key pair.\n";

            if (CryptGenKey(providerForCreateContainer, AT_KEYEXCHANGE, 0, &exchangeKey)) {
                std::cout << "Exchange key pair created.\n";
            } else {
                std::cout << "Error creating exchange key.\n";
                return false;
            }
        } else {
            std::cout << "An error other than NTE_NO_KEY at CryptGetUserKey().\n";
            return false;
        }
    }

    // Освобождаем дескриптор ключа обмена.
    std::cout << "\nDestroing exchange key.\n";

    if (!CryptDestroyKey(exchangeKey) ) {
        std::cout << "Error at CryptDestroyKey().\n";
        return false;
    }
    std::cout << "Key has been destroyed.\n";

    // Освобождаем дескриптор провайдера.
    std::cout << "\nReleasing provider's context.\n";

    if (!CryptReleaseContext(providerForCreateContainer, 0)) {
        std::cout << "Error at CryptReleaseContext().\n";
    }
    std::cout << "Context released.\n";
    return true;
}

bool CryptoProvider::hasContainer() const {
    return containerIsImported;
}


