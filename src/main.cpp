#include "base_widget.h"
#include "vipnet_provider.h"
#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    ViPNetProvider cryptoProvider;
    BaseWidget w(&cryptoProvider);
    w.show();

    return a.exec();
}
