#ifndef CRYPTO_PROVIDER_H
#define CRYPTO_PROVIDER_H

#include <wincrypt.h>
#include <cspsdk/importitccsp.h>
#include <iostream>
#include <vector>
#include "crypto_cipher.h"
#include "crypto_hash.h"
#include "crypto_signature.h"
#include <QObject>

#include <QList>
#include <QPair>

class CryptoProvider {

public:
    CryptoProvider();
    bool init(char *containerName);
    bool createContainer(char *containerName, int type);
    bool hasContainer() const;
    CryptoCipher cipher;
    CryptoHash hash;
    CryptoSignature sign;
    QList<QPair<int, QString>> providerType;

protected:
    HCRYPTPROV provider = 0;
    QList<QPair<int, QString>> hashAlgorithms;
    QList<QPair<int, QString>> signAlgorithms;
    QList<QPair<int, QString>> keyGenAlg;
    bool containerIsImported = false;
};

#endif // CRYPTO_PROVIDER_H
